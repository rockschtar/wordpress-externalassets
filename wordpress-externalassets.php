<?php
/**
 * Plugin Name: WordPress External Assets
 * Plugin URI: https://gitlab.com/rockschtar/wordpress-externalassets
 * Description: Must use plugin. Providers helper functions to register and download external scripts and styles locally. Useful for GDPR issues.
 * Version: develop
 * Author: Stefan Helmer
 * Author URI: https://gitlab.com/rockschtar/
 * License: MIT License
 */

use Rockschtar\WordPress\ExternalAssets\Controller\CronController;
use Rockschtar\WordPress\ExternalAssets\ExternalAssets;

define('RWEA_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('RWEA_PLUGIN_FILE', __FILE__);

if (file_exists(RWEA_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php')) {
    require_once 'vendor/autoload.php';
}

CronController::init();

/**
 * @param string $handle
 * @param string $remote_source
 * @param array $deps
 * @param mixed $ver
 * @param bool $in_footer
 */
function rsea_register_script(string $handle, string $remote_source, array $deps = array(), $ver = false, $in_footer = false) {
    ExternalAssets::registerScript($handle, $remote_source, $deps, $ver, $in_footer);
}

/**
 * @param string $handle
 * @param string $remote_source
 * @param array $deps
 * @param bool $ver
 * @param string $media
 */
function rsea_register_style(string $handle, string $remote_source, array $deps = array(), $ver = false, string $media = 'all') {
    ExternalAssets::registerStyle($handle, $remote_source, $deps, $ver, $media);
}

function resea_update_asset(string $handle) {
    ExternalAssets::update($handle);
}

function resea_update_assets() {
    ExternalAssets::update();
}

