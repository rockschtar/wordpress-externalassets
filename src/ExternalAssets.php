<?php

namespace Rockschtar\WordPress\ExternalAssets;

use League\Uri\Parser;
use Rockschtar\WordPress\ExternalAssets\Models\Asset;

class ExternalAssets {
    protected static $_instance;

    /**
     * @var Asset[]
     */
    private $assets;

    private static function instance(): ExternalAssets {
        if (null === self::$_instance) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    protected function __clone() {
    }

    public static function update(string $handle = null): void {

        foreach (self::instance()->assets as $asset) {

            if ($handle === null || $asset->getHandle() === $handle) {
                self::instance()->download($asset);
            } else {
                continue;
            }

        }
    }


    public static function registerScript(string $handle, string $remote_source, array $deps = array(), $ver = false, $in_footer = false): bool {
        $asset = self::instance()->registerAsset($handle, $remote_source);
        return wp_register_script($handle, $asset->getLocalSource(), $deps, $ver, $in_footer);
    }

    public static function registerStyle(string $handle, string $remote_source, array $deps = array(), $ver = false, $media = 'all'): bool {
        $asset = self::instance()->registerAsset($handle, $remote_source);
        return wp_register_style($handle, $asset->getLocalSource(), $deps, $ver, $media);
    }

    private function registerAsset(string $handle, string $remote_source): Asset {

        $uri_parser = new Parser();
        $uri_parser_result = $uri_parser->parse($remote_source);

        $remote_filename = basename($uri_parser_result['path']);
        $remote_path = str_replace($remote_filename, '', $uri_parser_result['path']);
        $local_filename = sanitize_key($uri_parser_result['host']) . '_' . sanitize_key($remote_path) . '_' . $remote_filename;

        $wp_upload_dir = wp_upload_dir();
        $wp_upload_base_dir = $wp_upload_dir['basedir'];
        $wp_upload_base_url = $wp_upload_dir['baseurl'];

        $local_file = $wp_upload_base_dir . DIRECTORY_SEPARATOR . $local_filename;
        $local_source = $wp_upload_base_url . '/' . $local_filename;

        $asset = Asset::create($handle, $remote_source, $local_source, $local_file);

        $download = false;
        $update_on_demand = apply_filters('rswpea_update_on_demand', false);
        $update_on_demand = apply_filters('rswpea_update_on_demand_' . $handle, $update_on_demand);

        $local_file_exists = file_exists($local_file);

        if ($local_file_exists && $update_on_demand) {
            $filetime = filemtime($local_file);
            $cache_time = apply_filters('rswpea_cache_time', MONTH_IN_SECONDS);
            $cache_time = apply_filters('rswpea_cache_time_' . $handle, $cache_time);

            if ((time() - $filetime) > $cache_time) {
                $download = true;
            }
        } elseif (!$local_file_exists) {
            $download = true;
        }

        if ($download) {
            $this->download($asset);
        }

        $this->assets[] = $asset;


        return $asset;
    }

    private function download(Asset $asset): string {
        $connTimeout = 10;
        $url = parse_url($asset->getRemoteSource());
        $host = $url['host'];
        $path = $url['path'] ?? '/';

        if (isset($url['query'])) {
            $path .= '?' . $url['query'];
        }

        $port = $url['port'] ?? '80';

        $fp = @fsockopen($host, $port, $errno, $errstr, $connTimeout);

        if (!$fp) {
            if (file_exists($asset->getLocalFile())) {
                readfile($asset->getLocalFile());
            }

            /** @noinspection ForgottenDebugOutputInspection */
            error_log('RS External Assets: Could not download file from ' . $asset->getRemoteSource() . '. Errornumber: ' . $errno . '. Message:' . $errstr);
        } else {
            $header = "GET $path HTTP/1.0\r\n";
            $header .= "Host: $host\r\n";
            $header .= "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6\r\n";
            $header .= "Accept: */*\r\n";
            $header .= "Accept-Language: en-us,en;q=0.5\r\n";
            $header .= "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7\r\n";
            $header .= "Keep-Alive: 300\r\n";
            $header .= "Connection: keep-alive\r\n";
            $header .= "Referer: http://$host\r\n\r\n";
            fwrite($fp, $header);
            $response = '';

            while ($line = fread($fp, 4096)) {
                $response .= $line;
            }

            fclose($fp);
            $pos = strpos($response, "\r\n\r\n");
            $response = substr($response, $pos + 4);

            if (!file_exists($asset->getLocalFile())) {
                // Try to create the file, if doesn't exist
                fopen($asset->getLocalFile(), 'wb');
            }

            if (is_writable($asset->getLocalFile())) {
                /** @noinspection NestedPositiveIfStatementsInspection */
                if ($fp = fopen($asset->getLocalFile(), 'wb')) {
                    fwrite($fp, $response);
                    fclose($fp);
                }
            }
        }

        return $asset->getLocalFile();
    }
}