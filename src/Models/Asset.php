<?php


namespace Rockschtar\WordPress\ExternalAssets\Models;


class Asset {

    /**
     * @var string
     */
    private $handle;

    /**
     * @var string
     */
    private $remoteSource;

    /**
     * @var string
     */
    private $localFile;

    /**
     * @var string
     */
    private $localSource;


    /**
     * Asset constructor.
     * @param string $handle
     * @param string $remoteSource
     * @param string $localSource
     * @param string $localFile
     */
    public function __construct(string $handle, string $remoteSource, string $localSource, string $localFile) {
        $this->handle = $handle;
        $this->remoteSource = $remoteSource;
        $this->localFile = $localFile;
        $this->localSource = $localSource;
    }

    public static function create(string $handle, string $remoteSource, string $localSource, string $localFile): Asset {
        return new self($handle, $remoteSource, $localSource, $localFile);
    }

    /**
     * @return string
     */
    public function getHandle(): string {
        return $this->handle;
    }

    /**
     * @return string
     */
    public function getRemoteSource(): string {
        return $this->remoteSource;
    }

    /**
     * @return string
     */
    public function getLocalFile(): string {
        return $this->localFile;
    }

    /**
     * @return string
     */
    public function getLocalSource(): string {
        return $this->localSource;
    }

    /**
     * @param string $localSource
     * @return Asset
     */
    public function setLocalSource(string $localSource): Asset {
        $this->localSource = $localSource;
        return $this;
    }


}