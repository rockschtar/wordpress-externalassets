<?php


namespace Rockschtar\WordPress\ExternalAssets\Controller;


use Rockschtar\WordPress\ExternalAssets\ExternalAssets;

class CronController {
    private function __construct() {
        register_activation_hook(RWEA_PLUGIN_FILE, array(&$this, 'schedule'));
        register_deactivation_hook(RWEA_PLUGIN_FILE, array(&$this, 'unschedule'));
        add_action('rsea_update_assets', array(&$this, 'update_assets'));
        add_action('admin_action_rsea_update_assets', array(&$this, 'update_assets'));
    }

    public static function &init() {
        static $instance = false;
        if (!$instance) {
            $instance = new self();
        }

        return $instance;
    }

    final public function schedule(): void {
        wp_schedule_event(time(), 'weekly', 'rsea_update_assets');
    }

    final public function unschedule(): void {
        wp_clear_scheduled_hook('rsea_update_assets');
    }

    final public function update_assets(): void {
        $cron_update = apply_filters('rsea_cron_update', true);

        if ($cron_update) {
            ExternalAssets::update();
        }
    }


}